package utilities;

import model.AnnotationTest;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;

public class Utilities {
    /**
     * Search fields in class,that have specific annotation
     *
     * @param neededFieldAnnotationClass Class of searched annotation
     * @param examinedClass              Class in which we search fields
     * @return list of field names
     */
    public static List<String> getAnnotatedField(Class neededFieldAnnotationClass, Class examinedClass) {
        List<String> fieldNames = new ArrayList<>();
        Field[] fields = examinedClass.getDeclaredFields();

        for (Field field : fields) {
            if (field.getAnnotation(neededFieldAnnotationClass) != null) {
                fieldNames.add(field.getName());
            }
        }
        return fieldNames;
    }

    /**
     * Method print all annotated fields of a given class with field names and value
     *
     * @param examinedClass Class in which we search fields
     */
    public static void printFieldAnnotationValues(Class examinedClass) {
        Field[] fields = examinedClass.getDeclaredFields();
        System.out.println("Class: " + examinedClass.getName() + " has next annotated fields: ");
        for (Field field : fields) {
            if (field.getAnnotations().length > 0) {
                System.out.print(field.getName() + " with annotation: ");
                Annotation[] annotations = field.getAnnotations();
                for (Annotation an : annotations) {
                    System.out.println(an);
                }
            }
        }
    }

    /**
     * Method invoke methods of given class with given annotation
     * it is special conditions for serching methods. Need more details for other methods
     *
     * @param currentClass          given class
     * @param neededAnnotationClass given annotation for methods
     */
    public static void invokeMethods(Class currentClass, Class<? extends Annotation> neededAnnotationClass)
            throws IllegalAccessException, InstantiationException, InvocationTargetException {
        Object classInstance = currentClass.newInstance();
        Method[] methods = currentClass.getDeclaredMethods();
        System.out.println("Class " + currentClass.getName() + " has " + methods.length + " methods");
        for (Method method : methods) {
            method.setAccessible(true);
//            System.out.println(method.getName());
//            System.out.println(method.getAnnotation(neededAnnotationClass));
            if (method.isAnnotationPresent(neededAnnotationClass)) {
                System.out.println("Methode name is: " + method.getName());
                System.out.print("Result of invoked method is: ");
                Parameter[] params = method.getParameters();
                if (params.length == 0) {
                    method.invoke(classInstance);
                } else if (params.length == 1 && params[0].getType().equals(String.class)) {
                    method.invoke(classInstance, "Print message method invoked");
                } else if (params.length == 2 && params[0].getType().equals(int.class) && params[1].getType().equals(int.class)) {
                    System.out.println(method.invoke(classInstance, 4, 5));
                }

            }
        }
    }

    /**
     * method set field value (if int - to 5, if String - to "message was set")
     *
     * @param current class for setting field value
     * @return object with changed fields
     */
    public static Object setFieldValues(Class current) throws IllegalAccessException, InstantiationException {
        Object classInstance = current.newInstance();
        Field[] fields = current.getDeclaredFields();
        System.out.println("Class " + current.getName() + " has " + fields.length + " fields");
        for (Field field : fields) {
            System.out.println("Name: " + field.getName() + " with type: " + field.getType());
            field.setAccessible(true);
            if (field.getType().equals(int.class)) {
                field.setInt(classInstance, 5);
                System.out.println("Setted value is :" + ((AnnotationTest) classInstance).getNumber());
            } else if (field.getType().equals(String.class)) {
                field.set(classInstance, "message was set");
                System.out.println("Setted value is :" + ((AnnotationTest) classInstance).getMessage());
            }
        }
        return classInstance;
    }

    /**
     * Method invoke given method (only with string or int parameters) in given class
     * put default values in invoked method
     * (works only for myMethod (String...args) and myMethod (String word, int...args))
     *
     * @param current        class, from witch method will be invoke
     * @param methodName
     * @param parameterTypes
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     */
    public static void invokeGivenMethods(Class current, String methodName, Class<?>... parameterTypes)
            throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        Object classInstance = current.newInstance();
        Method method = current.getMethod(methodName, parameterTypes);
        if (method != null) {
            method.setAccessible(true);
            System.out.println("Method name is: " + method.getName());
            System.out.println(method.getParameters().length);
            if (parameterTypes.length == 1 && parameterTypes[0].equals(String[].class)) {
                String[] words = {"This", "was", "invoked", "method"};
                method.invoke(classInstance, new Object[]{words});
            } else if (parameterTypes.length == 2 && parameterTypes[0].equals(String.class)
                    && parameterTypes[1].equals(int[].class)) {
                int[] numbers = {1, 2, 3};
                method.invoke(classInstance, "This was invoked method", numbers);
            }
        }
    }
}