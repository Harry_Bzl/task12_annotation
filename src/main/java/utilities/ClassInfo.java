package utilities;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;

public class ClassInfo {
    private Object obj;
    private Class<?> currentClass;

    public ClassInfo(Object obj) {
        this.obj = obj;
        currentClass = obj.getClass();
    }

    public void printFields(){
        Field [] fields = currentClass.getDeclaredFields();
        System.out.println("Fields of class " + currentClass.getSimpleName() + " are: ");
        if (fields.length==0||fields==null) System.out.println("No declared fields in class.");
        else {
            for (int i = 1 ; i<=fields.length; i++){
                Field field = fields[i-1];
                System.out.print(i + ") ");
                for (Annotation an:field.getAnnotations()) System.out.println(an.toString());
                System.out.print(Modifier.toString(field.getModifiers()));
                System.out.print(" " + field.getType().getName()+ " " + field.getName());
                System.out.println();
            }
        }
    }

    public void printConstructors(){
        System.out.println("Constructors of class " + currentClass.getSimpleName() + " are: ");
        Constructor [] constructors = currentClass.getDeclaredConstructors();
        if (constructors.length==0||constructors==null)System.out.println("No declared constructors.");
        else {
            for (int i = 1; i<=constructors.length; i++){
                System.out.print(i + ") ");
                Constructor constr = constructors[i-1];
                for (Annotation an:constr.getDeclaredAnnotations())System.out.println(an);
                System.out.print(Modifier.toString(constr.getModifiers())+ " ");
                System.out.print(constr.getName() + "(");
                for (Parameter param: constr.getParameters())System.out.print(param.getType().getSimpleName()+ " "
                + param.getName()+", ");
                System.out.println(")");
            }
        }
    }

    public void printMethods(){
        System.out.println("Declared methods of class " + currentClass.getSimpleName() + " are: ");
        Method[] methods = currentClass.getDeclaredMethods();
        if (methods.length==0||methods==null)System.out.println("No declared methods.");
        else {
            for (int i = 1; i<=methods.length; i++){
                System.out.print(i + ") ");
                Method method = methods[i-1];
                for (Annotation an:method.getDeclaredAnnotations())System.out.println(an);
                System.out.print(Modifier.toString(method.getModifiers())+ " ");
                System.out.print(method.getName() + "(");
                for (Parameter param: method.getParameters())System.out.print(param.getType().getSimpleName()+ " "
                        + param.getName()+", ");
                System.out.println(")");
            }
        }

    }

    public void printClassInfo (){
        for (Annotation an:currentClass.getAnnotations())System.out.println(an.toString());
        System.out.print(currentClass.getName());
        if (currentClass.getInterfaces().length>0){
            System.out.print(" implements ");
            for (Class cl:currentClass.getInterfaces())System.out.print(cl.getSimpleName()+ " ");
        }
        System.out.println();
        printFields();
        printConstructors();
        printMethods();
    }
}
