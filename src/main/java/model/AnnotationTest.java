package model;

import annotations.FieldAnnotation;
import annotations.MethodAnnotation;
import annotations.MyAnnotation;

import java.util.Random;

@MyAnnotation(value = "Value", number = 2)
public class AnnotationTest {
    private int number;
    @FieldAnnotation(value = "Custom value")
    private String message;
    @FieldAnnotation (value = "Custom object")
    private Object myObject;
    private Boolean counter;

    public AnnotationTest (){

    }

    public AnnotationTest(int number, String message) {
        this.number = number;
        this.message = message;
    }

    @MethodAnnotation
    public void printWord (String word){
        System.out.println(word);
    }

    @MethodAnnotation
    private int getSum (int firstNum, int secondNum){
        return firstNum + secondNum;
    }

    @MethodAnnotation
    public void printRandomNum (){
        System.out.println(new Random().nextInt());
    }

    public void setMyObject(Object myObject) {
        this.myObject = myObject;
    }

    public void setCounter(Boolean counter) {
        this.counter = counter;
    }

    public int getNumber() {
        return number;
    }

    public String getMessage() {
        return message;
    }

    public void myMethodStringInt (String message, int...args){
        System.out.print(message);
        for (int i:args)System.out.print(" " + i);
        System.out.println();
    }

    public void myMethodString (String...args){
        System.out.print("Message is: ");
        for (String i:args) System.out.print(" " + i);
    }
}
