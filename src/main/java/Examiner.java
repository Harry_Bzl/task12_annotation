import annotations.FieldAnnotation;
import annotations.MethodAnnotation;
import model.AnnotationTest;
import utilities.ClassInfo;
import utilities.Utilities;

import java.lang.reflect.InvocationTargetException;

public class Examiner {
    public static void main(String[] args) {
        System.out.print("Fields with custom annotation 'FieldAnnotation' for class:");
        System.out.println(AnnotationTest.class.getName());
        for (String field: Utilities.getAnnotatedField(FieldAnnotation.class, AnnotationTest.class)){
            System.out.println(field);
        }
        System.out.println("---------------------------------------");

        System.out.println("Get field annotation values");
        Utilities.printFieldAnnotationValues(AnnotationTest.class);
        System.out.println("---------------------------------------");

        try {
            System.out.println("Invoke methods with annotation 'MethodAnnotation' ");
            Utilities.invokeMethods(AnnotationTest.class, MethodAnnotation.class);
            System.out.println("---------------------------------------");
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }

        System.out.println("Set field values into class");
        try {
            Utilities.setFieldValues(AnnotationTest.class);
        } catch (IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
        System.out.println("---------------------------------------");


        System.out.println("Invoke myMethod (String str, int...args)");
        try {
            Utilities.invokeGivenMethods(AnnotationTest.class, "myMethodStringInt",
                    String.class, int[].class);
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        System.out.println("---------------------------------------");


        System.out.println("Invoke myMethod (String...args)");
        try {
            Utilities.invokeGivenMethods(AnnotationTest.class, "myMethodString", String[].class);
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        System.out.println();
        AnnotationTest anTest = new AnnotationTest();
        ClassInfo classInfo = new ClassInfo(anTest);
        classInfo.printClassInfo();
    }

}
